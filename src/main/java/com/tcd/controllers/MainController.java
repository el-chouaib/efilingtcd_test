package com.tcd.controllers;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
//import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;



@RestController
public class MainController {

	
	//static final Logger logger = Logger.getLogger(MainController.class);

	/*
	    Depuis la TCD, L'utilisateur connecté (USER) selectionne une compagnie et d'une année fiscale (company + year = TRD)
		Au clic sur le bouton "e-filling" on arrivera sur le lien http://localhost:8080/EFiling/ ( Une fois en production ce sera qqch comme
		http://intranet:8080/EFiling/)  avec en paramètres USER et TRD (à confirmer si param GET /POST) qui contiendront des valeures cryptées MD5.
		L'ouverture de ce nouveau onglet entraine l'appel à plusieurs webservice :
		~TCD/requests/employees/1611 (avec la valeur USER cryptée) pour récupérer les permissions
		~TCD/requests/declarations/trd/3400 (avec la valeur TRD cryptée) pour récupérer l'année fiscale et l'id de la compagnie
		~TCD/requests/companies/1108 (avec l'id de la compagnie) pour récupérer le nom de la compagnie
		En parallèle, il faut récupérer le cookie d'authentification de l'intranet pour pouvoir logger l'utilisateur. (penser à verifier les date d'expiration de la session Intranet et de la session Tomcat Efiling)
		 
		Une fois ces requetes effectuées, l'utilisateur (cas d'un non-admin) arrivera directement sur la page "app/formList" en full screen sans sidebar, sans possibilité de changer la company ni le user connecté, ni possibilité de faire de logout. Sur cette page il faudra aussi mentionner à l'utilisateur s'il y a déjà des décla créées pour ce TRD et si besoin de plus de détails on appelera le webservice
		~TCD/requests/companies/declarations/1022
		 
		Ensuite si on souhaite faire une nouvelle décla on appelera le webservice
		~TCD/requests/companies/references/12 (avec l'id de la company crypté) pour recupérer les informations de la compagnie pour le préremplissage.
	 */
	
	
	
	// Retourne l'employé selectionné par son id - Methode temporaire 
	@GetMapping(value = "/requests/employees/{uid}",produces= {MediaType.APPLICATION_JSON_VALUE})
	public String getEmployeeById(@PathVariable Integer uid)  {
		
		String json = "{\"data\":{\"id\":"+uid+",\"name\":\"Nicolas Ambroise\",\"mail\":\"nicolas.ambroise@fiad.lu\",\"rank\":2,\"function\":13,\"initials\":\"NIA\"}}";
		return json;
	}
	
	
	// Retourne la compagnie par id
	@GetMapping(value = "/requests/companies/{cid}",produces= {MediaType.APPLICATION_JSON_VALUE})
	public String getCompaniesById(@PathVariable Integer cid)  {

		String json = "{\"data\":{\"id\":"+cid+",\"company_name\":\"Atoz Services TEST\",\"apporteur_code\":\"AT\",\"fiscal_number\":\"20052121212\","
				+ "\"fiscal_years\":\"3255,2009;10,2010;11,2011;2896,2012;5073,2013;6092,2014;12470,2015;17382,2016;19098,2017;24225,2018;\"}}";
		return json;
	}
	
	// Retourne les références par id 
	@GetMapping(value = "/requests/references/{rid}",produces= {MediaType.APPLICATION_JSON_VALUE})
	public String getCompanyRefById(@PathVariable Integer rid)  {

		String json = "{\"data\":{\"id\":"+rid+",\"company\":8057,\"company_address_num\":\"20\",\"company_address_street\":\"rue du Findel\","
				+ "\"company_address_compl\":\"\",\"company_address_postcode\":\"L-2535\",\"company_address_city\":\"Luxembourg\",\"company_address_tel\":\"\","
				+ "\"company_address_mail\":\"\",\"legal_name\":\"Smith\",\"legal_firstname\":\"John\","
				+ "\"legal_matricule\":\"\",\"legal_address_num\":\"42\",\"legal_address_street\":\"rue du Nord \",\"legal_address_compl\":\"App. 72\","
				+ "\"legal_address_postcode\":\"28000\",\"legal_address_city\":\"Madrid\",\"legal_address_tel\":\"\",\"legal_address_mail\":\"\","
				+ "\"postal_address_num\":null,\"postal_address_street\":null,\"postal_address_compl\":null,\"postal_address_postcode\":null,\"postal_address_city\":null,"
				+ "\"impot_desk\":\"S6\",\"file_number\":\"20050000001\",\"rcs_number\":\"B000001\",\"company_object\":\"Société d'investissement en capital à risque.\","
				+ "\"iban_code\":\"\",\"bic_code\":\"\",\"company_format\":\"23\",\"company_bourse\":0,\"company_currency\":\"EUR\",\"company_type_currency\":0,"
				+ "\"legal_birthdate_formatted\":\"01/01/1990\",\"legal_birthplace\":\"Barcelone\","
				+ "\"company_date_creation_formatted\":\"01/01/2005\",\"company_name\":\"Atoz Services TEST\",\"company_address_country\":\"LU\","
				+ "\"legal_address_country\":\"ES\",\"postal_address_country\":null,\"number_format\":3,\"apporteur_code\":\"AT\",\"idPartnerATOZ\":-1,\"idPartnerAS\":-1}}";
		return json;
	}
	
	
	// Retourne la liste de toutes les références par Company
	@GetMapping(value = "/requests/references/company/{cid}",produces= {MediaType.APPLICATION_JSON_VALUE})
	public String getCompanyRefByCompany(@PathVariable Integer cid)  {

		String json = "{\"data\":[{\"id\":"+cid+",\"company\":8057,\"company_address_num\":\"20\",\"company_address_street\":\"rue du Findel\","
				+ "\"company_address_compl\":\"\",\"company_address_postcode\":\"L-2535\",\"company_address_city\":\"Luxembourg\",\"company_address_tel\":\"\","
				+ "\"company_address_mail\":\"\",\"legal_name\":\"Smith\",\"legal_firstname\":\"John\","
				+ "\"legal_matricule\":\"\",\"legal_address_num\":\"42\",\"legal_address_street\":\"rue du Nord \",\"legal_address_compl\":\"App. 72\","
				+ "\"legal_address_postcode\":\"28000\",\"legal_address_city\":\"Madrid\",\"legal_address_tel\":\"\",\"legal_address_mail\":\"\","
				+ "\"postal_address_num\":null,\"postal_address_street\":null,\"postal_address_compl\":null,\"postal_address_postcode\":null,\"postal_address_city\":null,"
				+ "\"impot_desk\":\"S6\",\"file_number\":\"20050000001\",\"rcs_number\":\"B000001\",\"company_object\":\"Société d'investissement en capital à risque.\","
				+ "\"iban_code\":\"\",\"bic_code\":\"\",\"company_format\":\"23\",\"company_bourse\":0,\"company_currency\":\"EUR\",\"company_type_currency\":0,"
				+ "\"legal_birthdate_formatted\":\"01/01/1990\",\"legal_birthplace\":\"Barcelone\","
				+ "\"company_date_creation_formatted\":\"01/01/2005\",\"company_name\":\"Atoz Services TEST\",\"company_address_country\":\"LU\","
				+ "\"legal_address_country\":\"ES\",\"postal_address_country\":null,\"number_format\":3,\"apporteur_code\":\"AT\",\"idPartnerATOZ\":-1,\"idPartnerAS\":-1}]}";
		return json;
	
	}
	
	
	// Retourne la déclaration par son id
	@GetMapping(value = "/requests/declarations/{did}",produces= {MediaType.APPLICATION_JSON_VALUE})
	public String getDeclaById(@PathVariable Integer did)  {

		String json = "{\"data\":[]}";
		
		if(did == 21200) {
			json="{\"data\":{\"id\":"+did+",\"fk_template\":19,\"visible\":1,\"status\":3,\"trd\":19098,\"company\":4,"
					+ "\"title\":\"Déclaration pour l'impôt sur le revenu, l'impôt commercial et l'impôt sur la fortune des collectivités résidentes\","
					+ "\"label\":\"500F_2017_fr\",\"company_name\":\"ATOZ Services TEST\",\"annexe\":\"\",\"annexe_type\":\"\",\"creator\":\"GKO\","
					+ "\"creatorId\":1611,\"year\":2017,\"updater\":\"JME\"}}";
		}
		else if(did == 22313) {
			json="{\"data\":{\"id\":"+did+",\"fk_template\":21,\"visible\":1,\"status\":3,\"trd\":19098,\"company\":4,"
					+ "\"title\":\"Annexe 500F_2017  - Personne Morale\",\"label\":\"500F_2017_fr_AM\",\"company_name\":\"ATOZ Services TEST\","
					+ "\"annexe\":\"Annexe TEST #1\",\"annexe_type\":\"AM\",\"creator\":\"GKO\",\"creatorId\":1611,\"year\":2017,\"updater\":\"JME\"}}";
		}
		else if(did == 21200) {
			json="{\"data\":{\"id\":"+did+",\"fk_template\":21,\"visible\":1,\"status\":3,\"trd\":19098,\"company\":4,\"title\":\"Annexe 500F_2017  - Personne Morale\","
					+ "\"label\":\"500F_2017_fr_AM\",\"company_name\":\"ATOZ Services TEST\",\"annexe\":\"Annexe TEST #2\",\"annexe_type\":\"AM\","
					+ "\"creator\":\"JME\",\"creatorId\":1611,\"year\":2017,\"updater\":\"NIA\"}}";
		}
		else if(did == 21200) {
			json="{\"data\":{\"id\":"+did+",\"fk_template\":21,\"visible\":1,\"status\":3,\"trd\":19098,\"company\":4,\"title\":\"Annexe 500F_2017  - Personne Morale\","
					+ "\"label\":\"500F_2017_fr_AM\",\"company_name\":\"ATOZ Services TEST\",\"annexe\":\"Annexe TEST #3\",\"annexe_type\":\"AM\","
					+ "\"creator\":\"JME\",\"creatorId\":1611,\"year\":2017,\"updater\":\"NIA\"}}";
		}
		return json;
	}
	
	// Retourne la liste de toutes les déclarations par TRD
	@GetMapping(value = "/requests/declarations/trd/{tid}",produces= {MediaType.APPLICATION_JSON_VALUE})
	public String getDeclaByTrd(@PathVariable Integer tid)  {

		String json = "{\"data\":[]}";
		
		if(tid == 19098) {
			json = "{\"data\":[{\"id\":21200,\"fk_template\":19,\"visible\":1,\"status\":3,\"trd\":"+tid+",\"company\":4,"
					+ "\"title\":\"Déclaration pour l'impôt sur le revenu, l'impôt commercial et l'impôt sur la fortune des collectivités résidentes\","
					+ "\"label\":\"500F_2017_fr\",\"company_name\":\"ATOZ Services TEST\",\"annexe\":\"\",\"annexe_type\":\"\",\"creator\":\"GKO\","
					+ "\"creatorId\":1611,\"year\":2017,\"updater\":\"JME\"},"
					+ "{\"id\":22313,\"fk_template\":21,\"visible\":1,\"status\":3,\"trd\":19098,\"company\":4,"
					+ "\"title\":\"Annexe 500F_2017  - Personne Morale\",\"label\":\"500F_2017_fr_AM\",\"company_name\":\"ATOZ Services TEST\","
					+ "\"annexe\":\"Annexe TEST #1\",\"annexe_type\":\"AM\",\"creator\":\"GKO\",\"creatorId\":1611,\"year\":2017,\"updater\":\"JME\"},"
					+ "{\"id\":22314,\"fk_template\":21,\"visible\":1,\"status\":3,\"trd\":19098,\"company\":4,\"title\":\"Annexe 500F_2017  - Personne Morale\","
					+ "\"label\":\"500F_2017_fr_AM\",\"company_name\":\"ATOZ Services TEST\",\"annexe\":\"Annexe TEST #2\",\"annexe_type\":\"AM\","
					+ "\"creator\":\"JME\",\"creatorId\":1611,\"year\":2017,\"updater\":\"NIA\"},"
					+ "{\"id\":22315,\"fk_template\":21,\"visible\":1,\"status\":3,\"trd\":19098,\"company\":4,\"title\":\"Annexe 500F_2017  - Personne Morale\","
					+ "\"label\":\"500F_2017_fr_AM\",\"company_name\":\"ATOZ Services TEST\",\"annexe\":\"Annexe TEST #3\",\"annexe_type\":\"AM\","
					+ "\"creator\":\"JME\",\"creatorId\":1611,\"year\":2017,\"updater\":\"NIA\"}]}";
		}
		return json;
	}
	
	// Retourne la liste de toutes les déclarations par Company
	@GetMapping(value = "/requests/declarations/company/{cid}",produces= {MediaType.APPLICATION_JSON_VALUE})
	public String getDeclaByCompany(@PathVariable Integer cid)  {

		String json = "{\"data\":[{\"id\":5645,\"fk_template\":12,\"visible\":1,\"status\":0,\"trd\":17382,\"company\":"+cid+","
				+ "\"title\":\"Déclaration pour l'impôt sur le revenu, l'impôt commercial et l'impôt sur la fortune des collectivités résidentes\","
				+ "\"label\":\"500F_2016_fr\",\"company_name\":\"ATOZ Services TEST\",\"annexe\":\"\",\"annexe_type\":\"\",\"creator\":\"GKO\","
				+ "\"creatorId\":1611,\"year\":2016,\"updater\":\"JME\"},"
				+ "{\"id\":21200,\"fk_template\":19,\"visible\":1,\"status\":3,\"trd\":19098,\"company\":"+cid+","
				+ "\"title\":\"Déclaration pour l'impôt sur le revenu, l'impôt commercial et l'impôt sur la fortune des collectivités résidentes\","
				+ "\"label\":\"500F_2017_fr\",\"company_name\":\"ATOZ Services TEST\",\"annexe\":\"\",\"annexe_type\":\"\",\"creator\":\"GKO\","
				+ "\"creatorId\":1611,\"year\":2017,\"updater\":\"JME\"},"
				+ "{\"id\":22313,\"fk_template\":21,\"visible\":1,\"status\":3,\"trd\":19098,\"company\":"+cid+","
				+ "\"title\":\"Annexe 500F_2017  - Personne Morale\",\"label\":\"500F_2017_fr_AM\",\"company_name\":\"ATOZ Services TEST\","
				+ "\"annexe\":\"Annexe TEST #1\",\"annexe_type\":\"AM\",\"creator\":\"GKO\",\"creatorId\":1611,\"year\":2017,\"updater\":\"JME\"},"
				+ "{\"id\":22314,\"fk_template\":21,\"visible\":1,\"status\":3,\"trd\":19098,\"company\":4,\"title\":\"Annexe 500F_2017  - Personne Morale\","
				+ "\"label\":\"500F_2017_fr_AM\",\"company_name\":\"ATOZ Services TEST\",\"annexe\":\"Annexe TEST #2\",\"annexe_type\":\"AM\","
				+ "\"creator\":\"JME\",\"creatorId\":1611,\"year\":2017,\"updater\":\"NIA\"},"
				+ "{\"id\":22315,\"fk_template\":21,\"visible\":1,\"status\":3,\"trd\":19098,\"company\":"+cid+",\"title\":\"Annexe 500F_2017  - Personne Morale\","
				+ "\"label\":\"500F_2017_fr_AM\",\"company_name\":\"ATOZ Services TEST\",\"annexe\":\"Annexe TEST #3\",\"annexe_type\":\"AM\","
				+ "\"creator\":\"JME\",\"creatorId\":1611,\"year\":2017,\"updater\":\"NIA\"}]}";
	
		return json;
	}

}